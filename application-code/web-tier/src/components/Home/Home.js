
    import React, {Component} from 'react';
    import architecture from '../../assets/techtide.png'

    class Home extends Component {
        render () {
        return (
            <div>
            <h1 style={{color:"white"}}>InfraMinds Tech Tide Demo</h1>
            <img src={architecture} alt="Flyer" style={{height:700,width:1000}} />
          </div>
        );
      }
    }

    export default Home;